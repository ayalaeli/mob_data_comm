package com.example.elisa.mob_data_project;

interface Observer {
	public void update(Observable o);
}