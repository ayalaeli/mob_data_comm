package com.example.elisa.mob_data_project;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkLoaderAndDisplay extends Thread {

    public String msg = "NO URL";

    //Then the thread can write on the activity
        public interface ThreadNotifier{
            public void threadProcessing(String m);
        }

        ThreadNotifier listener = null;

        public void SetThreadListener(ThreadNotifier l){
            listener = l;
        }

	public void run(){
        //listener.threadProcessing("Starting Thread");
		loadStuff();
	}

	public NetworkLoaderAndDisplay(String _msg){
	    msg = _msg;
    }

	private void loadStuff() {
      HttpURLConnection urlConnection = null;
      try{
        URL url = new URL(msg);
        //listener.threadProcessing("Get URL");


        urlConnection = (HttpURLConnection) url.openConnection();
        //listener.threadProcessing("Get URL Connection");


        InputStream in1 = urlConnection.getInputStream();
        //listener.threadProcessing("Get input stream");


        InputStream in = new BufferedInputStream(in1);
        //listener.threadProcessing("Reading stream");

        String allData = fromStream(in);
        //System.out.println(allData);
        listener.threadProcessing(allData);
        in.close();
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
      finally{
        if (urlConnection != null)
        {
          urlConnection.disconnect();
        }
      }
    }


    public static String fromStream(InputStream in) throws IOException
    {
      BufferedReader reader = new BufferedReader(new InputStreamReader(in));
      StringBuilder out = new StringBuilder();
      String newLine = System.getProperty("line.separator");
      String line;
      while ((line = reader.readLine()) != null) {
          out.append(line);
          out.append(newLine);
      }
      return out.toString();
  }


}