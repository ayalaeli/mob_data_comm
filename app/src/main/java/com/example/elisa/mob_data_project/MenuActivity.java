package com.example.elisa.mob_data_project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

@EActivity(R.layout.activity_menu)
public class MenuActivity extends AppCompatActivity implements MenuEngine.MenuDataAvailableInterface{

    //The engine which will load menu from json
    MenuEngine engine;

    @ViewById
    ListView list;

    @ViewById
    TextView text;

    @AfterViews
    void startEngine(){
        engine = new MenuEngine(this);
        engine.getMenuData();
    }

    @UiThread
    public void menuDataAvailable()
    {
        //Get menus from engine
        ArrayList<String> listItems = engine.getMenus();

        //Update the list with the data
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, listItems);
        list.setAdapter(adapter);

        //Set the text to the day of today
        text.setText(engine.getDay());
    }
}

