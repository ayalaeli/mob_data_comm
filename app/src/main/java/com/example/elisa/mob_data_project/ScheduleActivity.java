package com.example.elisa.mob_data_project;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;



@Fullscreen
@EActivity(R.layout.activity_schedule)
public class ScheduleActivity extends AppCompatActivity {

    @ViewById(R.id.imageView)
    ImageView img;

    /*protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        ImageView img = (ImageView) findViewById(R.id.imageView);
            Picasso.with(this).load("https://cloud.utc.fr/index.php/s/4ZUWBHny8DdtANC/download").into(img);
    }*/

    @AfterViews
    void loadImage(){
        Picasso.with(this).load("https://cloud.utc.fr/index.php/s/4ZUWBHny8DdtANC/download").into(img);
    }
}


