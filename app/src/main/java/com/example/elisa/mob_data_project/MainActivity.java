package com.example.elisa.mob_data_project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    //Grid model
    String[] web = {
            "ContactsActivity_",
            "MenuActivity_",
            "WeatherActivity",
            "ScheduleActivity_"
    } ;
    int[] imageId = {
            R.drawable.contacts,
            R.drawable.restaurant,
            R.drawable.weather,
            R.drawable.calendar
    };

    @ViewById
    GridView grid;

    @AfterViews
    void loadGrid(){
        CustomGrid adapter = new CustomGrid(MainActivity.this, web, imageId);
        grid.setAdapter(adapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try
                {
                    Intent myIntent = new Intent(MainActivity.this, Class.forName("com.example.elisa.mob_data_project."+web[position]));
                    MainActivity.this.startActivity(myIntent);
                }
                catch(Exception e)
                {
                    Log.i("MainActivity-APP",e.getMessage());
                }
            }
        });
    }
}
