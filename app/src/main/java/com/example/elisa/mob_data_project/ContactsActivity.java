package com.example.elisa.mob_data_project;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

@EActivity(R.layout.activity_contacts)
public class ContactsActivity extends AppCompatActivity {
    // JSON Node names
    private static final String CONTACTS = "contacts";
    private static final String NAME = "name";
    private static final String PHONE = "phone";

    @ViewById
    ListView list;

    @ViewById
    TextView text;

    @AfterViews
    protected void init()
    {
        // Generate correct URL according to date
        String url = "http://wwwetu.utc.fr/~ayalaeli/";

        // Get array from json and init the list
        getDataArray(url);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                String phone = "tel:"+((TextView) view.findViewById(R.id.phone)).getText().toString();
                call(phone);
            }});

    }

    protected void call(String callData) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL); //dial
        callIntent.setData(Uri.parse(callData));

        try{
            startActivity(callIntent);
        }
        catch (android.content.ActivityNotFoundException ex){
            Toast.makeText(getApplicationContext(),"yourActivity is not founded", Toast.LENGTH_SHORT).show();
        }
    }

    @UiThread
    protected void initList(ArrayList<HashMap<String, String>> cList)
    {

        ListAdapter adapter = new SimpleAdapter(
                ContactsActivity.this, cList,
                R.layout.contacts_item, new String[]{NAME, PHONE}, new int[]{R.id.name, R.id.phone});

        list.setAdapter(adapter);
    }


    @Background
    protected void getDataArray(String url)
    {

        ArrayList<HashMap<String, String>> contactsArray = new ArrayList<HashMap<String, String>>();

        try {
            JSONParser parser = new JSONParser();
            JSONObject json = parser.getDataJSON(url);
            JSONArray contacts = json.getJSONArray(CONTACTS);
            // looping through all contacts
            for(int i = 0; i < contacts.length(); i++){

                JSONObject person = contacts.getJSONObject(i);
                HashMap<String, String> l = new HashMap<String, String>();
                l.put(NAME, person.getString(NAME));
                l.put(PHONE, person.getString(PHONE));

                // adding HashList to ArrayList
                contactsArray.add(l);

            }
        } catch (Exception e) {
            Log.i("myapp", "Couldn't process JSON properly.");
            Log.i("myapp", e.getMessage());
        }

        initList(contactsArray);
    }



}
