package com.example.elisa.mob_data_project;


import android.util.Log;

class HTTPGetThread extends Thread implements Observable, NetworkLoaderAndDisplay.ThreadNotifier{

    Observer jsonMenuEngine;

    String data = "";

    String url = "";

    public HTTPGetThread(String _url){
        url = _url;
    }

    public void run(){
        NetworkLoaderAndDisplay nlad = new NetworkLoaderAndDisplay(url);
        nlad.SetThreadListener(this);
        nlad.start();
    }

    public void threadProcessing(final String m){
        try
        {
            data = m;
            notifyObservers();
        }
        catch (Exception e)
        {
            Log.i("MenuGetter",e.getMessage());
        }
    }

    @Override
    public void addObserver(Observer o) {
        jsonMenuEngine = o;
    }

    @Override
    public void deleteObserver(Observer o) {
        jsonMenuEngine = null;
    }

    @Override
    public void notifyObservers() {
        jsonMenuEngine.update(this);
    }

    public String getData(){
        return data;
    }
}