package com.example.elisa.mob_data_project;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class MenuEngine implements Observer
{
    ArrayList<String> menus;
    String day;

    @Override
    public void update(Observable o) {
        try
        {
            //Get the data from the thread
            String data = ((HTTPGetThread)o).getData();

            //Get main element:
            Map<String, Object> parsed = JSONParser.jsonToMap(new JSONObject(data));
            Map<String, Object> mainElement = (Map) parsed.get("LunchMenu");

            //Get the day of today
            day = (String)mainElement.get("DayOfWeek");

            //Get each menu
            ArrayList<Object> setMenus = (ArrayList)mainElement.get("SetMenus");

            menus = new ArrayList<String>();

            for(int i = 0; i<setMenus.size(); i++){
                String menu = "";
                ArrayList<Object> meals = (ArrayList)((Map)setMenus.get(i)).get("Meals");
                for(int j = 0; j<meals.size(); j++){
                    Map<String, Object> meal = (Map) meals.get(j);
                    menu += (String)meal.get("Name") + "\n";
                }
                if(meals.size() != 0){
                    menus.add(menu);
                }

            }

            //Notify the main activity
            uiCallback.menuDataAvailable();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    // This interface is used to report data back to UI
    public interface MenuDataAvailableInterface
    {
        // This method is called back in background thread.
        public void menuDataAvailable();
    }


    protected MenuDataAvailableInterface uiCallback;

    // Constructor
    public MenuEngine(MenuDataAvailableInterface callbackInterface)
    {
        this.uiCallback = callbackInterface;
    }

    public void getMenuData()
    {
        //Get the date of today with the proper format
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateandTime = sdf.format(new Date());

        //Get the url
        String url = "https://www.amica.fi/api/restaurant/menu/day?date="+ currentDateandTime +"&language=en&restaurantPageId=66287";

        //Get the data from the url with a thread by subscribing to it
        HTTPGetThread getter = new HTTPGetThread(url);
        getter.addObserver(this);
        getter.start();
    }

    public ArrayList<String> getMenus(){return menus;}

    public String getDay(){return day;}

}